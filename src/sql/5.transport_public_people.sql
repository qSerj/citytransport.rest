create table people
(
    id       serial
        constraint people_pk
            primary key,
    login    varchar(255),
    password varchar(10),
    balance  numeric(12, 2),
    role     varchar(10)
);

alter table people
    owner to postgres;

INSERT INTO public.people (login, password, balance, role) VALUES ('admin@transport.ru', '1234', 0.00, 'ADMIN');
INSERT INTO public.people (login, password, balance, role) VALUES ('petya@mail.ru', '1234', 500.00, 'USER');
INSERT INTO public.people (login, password, balance, role) VALUES ('vasya@mail.ru', '1234', 700.00, 'USER');
