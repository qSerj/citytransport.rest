create table transport_type
(
    id  serial
        constraint ts_type_pk
            primary key,
    type varchar(20)
);

alter table transport_type
    owner to postgres;

INSERT INTO public.transport_type (type) VALUES ('BICYCLE');
INSERT INTO public.transport_type (type) VALUES ('ELECTRIC_SCOOTER');
