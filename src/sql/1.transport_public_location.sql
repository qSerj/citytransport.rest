create table location
(
    id        serial
        constraint location_pk
            primary key,
    longitude varchar(40),
    latitude  varchar(40),
    title     varchar(255)
);

alter table location
    owner to postgres;

INSERT INTO public.location (longitude, latitude, title) VALUES ('35.87742138336606', '56.81790485680933', 'one');
INSERT INTO public.location (longitude, latitude, title) VALUES ('35.94496791945129', '56.845757046285264', 'two');
INSERT INTO public.location (longitude, latitude, title) VALUES ('35.83450603912639', '56.853378847878', 'three');
