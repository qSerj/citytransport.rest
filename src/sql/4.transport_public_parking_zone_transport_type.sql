create table parking_zone_transport_type
(
    transport_type_id integer not null
        constraint zone_to_ts_type_type_id_fkey
            references transport_type,
    parking_zone_id   integer not null
        constraint zone_to_ts_type_parking_zone_id_fkey
            references parking_zone,
    constraint zone_to_ts_type_pkey
        primary key (transport_type_id, parking_zone_id)
);

alter table parking_zone_transport_type
    owner to postgres;

INSERT INTO public.parking_zone_transport_type (transport_type_id, parking_zone_id) VALUES (1, 1);
INSERT INTO public.parking_zone_transport_type (transport_type_id, parking_zone_id) VALUES (2, 1);
INSERT INTO public.parking_zone_transport_type (transport_type_id, parking_zone_id) VALUES (2, 2);
INSERT INTO public.parking_zone_transport_type (transport_type_id, parking_zone_id) VALUES (1, 3);
INSERT INTO public.parking_zone_transport_type (transport_type_id, parking_zone_id) VALUES (2, 3);
