create table transport
(
    id                   serial
        constraint transport_pk
            primary key,
    state                varchar(20),
    serial               varchar(15),
    type_id              integer
        constraint transport_type_id_fkey
            references transport_type,
    condition            varchar(50),
    parking_zone_id      integer
        constraint transport_park_zone_id_fkey
            references parking_zone,
    charge_level_percent numeric(2),
    max_speed_kmh        integer,
    dtype                varchar
);

alter table transport
    owner to postgres;

INSERT INTO public.transport (state, serial, type_id, condition, parking_zone_id, charge_level_percent, max_speed_kmh, dtype) VALUES ('FREE', 'VEL-01', 1, 'good', 1, 0, 0, 'Bicycle');
INSERT INTO public.transport (state, serial, type_id, condition, parking_zone_id, charge_level_percent, max_speed_kmh, dtype) VALUES ('FREE', 'VEL-02', 1, 'excellent', 3, 0, 0, 'Bicycle');
INSERT INTO public.transport (state, serial, type_id, condition, parking_zone_id, charge_level_percent, max_speed_kmh, dtype) VALUES ('FREE', 'ESC-01', 2, 'good', 2, 90, 25, 'ElectricScooter');
INSERT INTO public.transport (state, serial, type_id, condition, parking_zone_id, charge_level_percent, max_speed_kmh, dtype) VALUES ('FREE', 'ESC-02', 2, 'good', 2, 90, 25, 'ElectricScooter');
