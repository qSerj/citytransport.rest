create table trip
(
    id                    serial
        constraint trip_pk
            primary key,
    state                 varchar(20),
    user_id               integer
        constraint trip_user_id_fkey
            references people,
    transport_id          integer
        constraint trip_transport_id_fkey
            references transport,
    start_timestamp       timestamp,
    end_timestamp         timestamp,
    total_price DECIMAL(10,2),
    start_parking_zone_id integer
        constraint trip_start_location_id_fkey
            references parking_zone,
    end_parking_zone_id   integer
        constraint trip_end_location_id_fkey
            references parking_zone
);

alter table trip
    owner to postgres;

INSERT INTO public.trip (state, user_id, transport_id, start_timestamp, end_timestamp, start_parking_zone_id, end_parking_zone_id, total_price) VALUES ('FINISHED', 2, 1, '2021-10-02 15:23:00.000000', '2021-10-02 15:58:01.000000', 1, 2, 100);
INSERT INTO public.trip (state, user_id, transport_id, start_timestamp, end_timestamp, start_parking_zone_id, end_parking_zone_id, total_price) VALUES ('FINISHED', 2, 1, '2021-10-02 10:22:00.000000', '2021-10-02 10:40:15.000000', 2, 1, 100);
INSERT INTO public.trip (state, user_id, transport_id, start_timestamp, end_timestamp, start_parking_zone_id, end_parking_zone_id, total_price) VALUES ('FINISHED', 3, 2, '2021-10-02 10:22:00.000000', '2021-10-02 10:40:15.000000', 2, 3, 102);
