create table parking_zone
(
    id          serial
        constraint parking_zone_pk
            primary key,
    name       varchar(50),
    location_id integer
        constraint parking_zone_location_id_fkey
            references location,
    radius_m    numeric(5, 2)
);

alter table parking_zone
    owner to postgres;

INSERT INTO public.parking_zone (name, location_id, radius_m) VALUES ('center', 1, 300.00);
INSERT INTO public.parking_zone (name, location_id, radius_m) VALUES ('south', 2, 200.00);
INSERT INTO public.parking_zone (name, location_id, radius_m) VALUES ('west', 3, 300.00);
