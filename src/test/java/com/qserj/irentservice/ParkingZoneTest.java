package com.qserj.irentservice;

import com.qserj.irentservice.access.LocationRepository;
import com.qserj.irentservice.access.ParkingZoneRepository;

import com.qserj.irentservice.entities.converters.LocationMapper;
import com.qserj.irentservice.entities.converters.ParkingZoneMapper;
import com.qserj.irentservice.entities.dto.LocationDto;
import com.qserj.irentservice.entities.dto.ParkingZoneDto;
import com.qserj.irentservice.entities.entity.Location;
import com.qserj.irentservice.entities.entity.ParkingZone;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ParkingZoneTest {
    @Test
    void FindAllParkingZoneTest(
            @Autowired ParkingZoneRepository parkingZoneRepository,
            @Autowired LocationRepository locationRepository,
            @Autowired LocationMapper lconv,
            @Autowired ParkingZoneMapper pconv) {
        System.out.println("Trying to get all parking zones from DB:");
        var l = parkingZoneRepository.findAll();
        for (var t : l) {
            System.out.println(t);
        }
        System.out.println();

//        System.out.println("Trying to modify");
//        ParkingZone pkz = l.get(2);
//        pkz.setRadius(200);
//        parkingZoneRepository.save(pkz);

        Location loc = new Location();
        loc.setLatitude("lat");
        loc.setLongitude("long");
        loc.setTitle("Huaha");
        locationRepository.save(loc);

        LocationDto d = lconv.map(loc);

        ParkingZone pkz = new ParkingZone();
        pkz.setName("Newwest");
        pkz.setRadius(155);
        pkz.setLocation(loc);
        parkingZoneRepository.save(pkz);

        ParkingZoneDto pkzDto = pconv.map(pkz);

//        System.out.println("Trying to get all parking zones from DB:");
//        for (var t : parkingZoneRepository.findAll()) {
//            System.out.println(t);
//        }
//        System.out.println();
    }
}
