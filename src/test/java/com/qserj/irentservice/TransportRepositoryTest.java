package com.qserj.irentservice;

import com.qserj.irentservice.access.*;
import com.qserj.irentservice.entities.converters.TransportMapper;
import com.qserj.irentservice.entities.dto.TransportAllFieldsDto;
import com.qserj.irentservice.entities.dto.TransportState;
import com.qserj.irentservice.entities.entity.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
public class TransportRepositoryTest {

    @Autowired
    TransportRepository transportRepo;

    @Autowired
    ParkingZoneRepository pzRepo;

    @Autowired
    TransportTypeRepository ttRepo;

    @Autowired
    TransportMapper converter;

    @Test
    @Transactional
    void FindAllTransportTest() {

        System.out.println("Trying to get all transport from DB:");
        var l = transportRepo.findAll();

        for ( var t : l ) {
            if (t instanceof Bicycle) {
                System.out.print("Велосипед: " + t);
            }
            else if (t instanceof ElectricScooter) {
                System.out.print("Электричка: " + t);
            }
            System.out.println();
        }
        System.out.println();

        ParkingZone parkingZone = pzRepo.getById(1L);
        TransportType transportType = ttRepo.getById(1L);

        Bicycle b = new Bicycle();
        b.setSerial("XXX");
        b.setState(TransportState.FREE);
        b.setCondition("dood");
        b.setParkingZone(parkingZone);
        b.setType(transportType);
        transportRepo.save(b);

        TransportAllFieldsDto t1 = converter.map((Transport)b);

        ElectricScooter e = new ElectricScooter();
        e.setSerial("Самокат-5");
        e.setState(TransportState.FREE);
        e.setCondition("good");
        e.setParkingZone(parkingZone);
        e.setType(transportType);
        e.setChargeLevelPercent(99);
        e.setMaxSpeedKmh(20);
        transportRepo.save(e);

        TransportAllFieldsDto t2 = converter.map((Transport)e);

        System.out.println("Trying to get all transport from DB:");
        l = transportRepo.findAll();
        for ( var t : l ) {
            if (t instanceof Bicycle) {
                System.out.print("Велосипед: " + t);
            }
            else if (t instanceof ElectricScooter) {
                System.out.print("Электричка: " + t);
            }
            System.out.println();
        }
        System.out.println();
    }
}
