package com.qserj.irentservice.entities.dto;

import com.qserj.irentservice.access.ParkingZoneRepository;
import com.qserj.irentservice.access.TransportRepository;
import com.qserj.irentservice.access.TripRepository;
import com.qserj.irentservice.access.UserRepository;
import com.qserj.irentservice.entities.converters.TripMapper;
import com.qserj.irentservice.entities.entity.Trip;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
@Slf4j
class TripDtoTest {

    @Test
    void MapToTripDto(@Autowired TripRepository tripRepo,
                      @Autowired TripMapper converter) {

        Trip trip = tripRepo.getById(1L);

        TripDto tripDto = converter.map(trip);

        assertEquals(trip.getId(), tripDto.getId());
        assertEquals(trip.getState(), tripDto.getStatus());
        assertEquals(trip.getUser().getId(), tripDto.getUser().getId());
        assertEquals(trip.getTransport().getId(), tripDto.getVehicle().getId());
        assertEquals(trip.getTransport().getSerial(), tripDto.getVehicle().getRegNumber());
        assertEquals(trip.getTotalPrice(), tripDto.getTotalPrice());

    }
}