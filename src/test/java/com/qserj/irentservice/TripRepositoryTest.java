package com.qserj.irentservice;

import com.qserj.irentservice.access.ParkingZoneRepository;
import com.qserj.irentservice.access.TransportRepository;
import com.qserj.irentservice.access.TripRepository;
import com.qserj.irentservice.access.UserRepository;
import com.qserj.irentservice.entities.converters.TripMapper;
import com.qserj.irentservice.entities.dto.TripDto;
import com.qserj.irentservice.entities.entity.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
@Slf4j
public class TripRepositoryTest {

    @Test
    void FindAllTest(@Autowired TripRepository repository) {
        log.info("Trying to get all trips from DB:");
        var l = repository.findAll();
        for (var t : l) {
            log.info(t.toString());
        }
    }

    @Test
    void AddNewTrip(@Autowired TripRepository tripRepo,
                    @Autowired UserRepository userRepository,
                    @Autowired TransportRepository transportRepo,
                    @Autowired ParkingZoneRepository parkingZoneRepository,
                    @Autowired TripMapper converter) {

        User user = userRepository.getById(2L);
        Transport transport = transportRepo.getById(2L);
        ParkingZone parkingZone = parkingZoneRepository.getById(1L);

        Trip trip = new Trip();
        trip.setState(TripState.INPROCESS);
        trip.setUser(user);
        trip.setTransport(transport);
        trip.setStartTimestamp(LocalDateTime.now());
        trip.setEndTimestamp(LocalDateTime.now());
        trip.setStartParkingZone(parkingZone);
        trip.setEndParkingZone(parkingZone);

        assertNull(trip.getId());

        tripRepo.save(trip);

        assertNotNull(trip.getId());

        log.info(trip.toString());
    }
}
