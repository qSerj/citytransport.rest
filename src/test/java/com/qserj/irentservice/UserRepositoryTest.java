package com.qserj.irentservice;

import com.qserj.irentservice.access.UserRepository;
import com.qserj.irentservice.entities.entity.User;
import com.qserj.irentservice.entities.entity.UserRole;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

@SpringBootTest
public class UserRepositoryTest {
    @Test
    void ListUsersList(@Autowired UserRepository usersRepo) {
        //список всех пользователей
        System.out.println("Trying to get all users from DB:");
        var l = usersRepo.findAll();
        for ( var t : l ) {
            System.out.println(t);
        }
        System.out.println();
    }

    @Test
    void BigTest(@Autowired UserRepository repository) {
        //список всех пользователей
        System.out.println("Trying to get all users from DB:");
        var l = repository.findAll();
        for ( var t : l ) {
            System.out.println(t);
        }
        System.out.println();
        System.out.println("Trying to add user");
        User user = new User();
        user.setLogin("new@qwerty.com");
        user.setPassword("111");
        user.setRole(UserRole.USER);
        user.setBalance(new BigDecimal("1000"));
        repository.save(user);
        System.out.println("Added user id: " + user.getId());
        System.out.println("Trying to get all users from DB:");
        l = repository.findAll();
        for ( var t : l ) {
            System.out.println(t);
        }
        System.out.println();
        System.out.println("Trying to delete user by Id");
        repository.deleteById(user.getId());
        System.out.println("Trying to get all users from DB:");
        l = repository.findAll();
        for ( var t : l ) {
            System.out.println(t);
        }
        System.out.println();
    }
}



