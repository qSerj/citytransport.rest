package com.qserj.irentservice.entities.dto;

import com.qserj.irentservice.entities.entity.UserRole;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class UserShortDto {
    private Long id;
    private String login;
    private String role;
    private BigDecimal balance;
}
