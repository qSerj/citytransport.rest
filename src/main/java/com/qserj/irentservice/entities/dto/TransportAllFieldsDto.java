package com.qserj.irentservice.entities.dto;

import com.qserj.irentservice.entities.entity.ParkingZone;
import com.qserj.irentservice.entities.entity.TransportType;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TransportAllFieldsDto {
    private Long id;
    private TransportState status;
    private String regNumber;
    private String type;
    private String condition;
    private ParkingZoneDto parking;
    private LocationDto geoPosition;
    private Integer chargePercent;
    private Integer maxKmPerHourSpeed;
}
