package com.qserj.irentservice.entities.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AreaDto {
    private LocationDto center;
    private Integer radiusInMeters;
}
