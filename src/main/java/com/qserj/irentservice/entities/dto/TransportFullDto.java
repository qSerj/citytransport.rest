package com.qserj.irentservice.entities.dto;

import com.qserj.irentservice.entities.entity.ParkingZone;
import com.qserj.irentservice.entities.entity.TransportType;
import com.qserj.irentservice.entities.entity.TripState;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TransportFullDto {
    private Long id;
    private TransportState state;
    private String serial;
    private TransportType type;
    private String condition;
    private ParkingZone parkingZone;
}
