package com.qserj.irentservice.entities.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LocationDto {
    private Long id;
    private String title;
    private String latitude;
    private String longitude;
}
