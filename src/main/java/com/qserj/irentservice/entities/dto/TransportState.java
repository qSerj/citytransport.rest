package com.qserj.irentservice.entities.dto;

public enum TransportState {
    FREE,
    BUSY,
    DELETED
}
