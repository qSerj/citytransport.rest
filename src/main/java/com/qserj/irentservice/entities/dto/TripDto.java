package com.qserj.irentservice.entities.dto;

import com.qserj.irentservice.entities.entity.ParkingZone;
import com.qserj.irentservice.entities.entity.Transport;
import com.qserj.irentservice.entities.entity.TripState;
import com.qserj.irentservice.entities.entity.User;
import lombok.Builder;
import lombok.Data;

import javax.persistence.criteria.CriteriaBuilder;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Builder
public class TripDto {
    private Long id;
    private TripState status;
    private LocalDateTime startDateTime;
    private LocalDateTime endDateTime;
    private UserShortDto user;
    private TransportShortDto vehicle;
    private ParkingZoneDto startParking;
    private ParkingZoneDto finishParking;
    private BigDecimal totalPrice;
}
