package com.qserj.irentservice.entities.dto;

import com.qserj.irentservice.entities.entity.UserRole;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Builder
public class UserFullDto {
    private Long id;

    @NotBlank
    @Size(min=5, max=255)
    private String  login;

    @NotBlank
    @Size(min=3, max=10)
    private String  password;

    private UserRole role;

    @Min(0)
    private Integer balance;
}
