package com.qserj.irentservice.entities.dto;

import com.qserj.irentservice.entities.entity.TransportType;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TransportShortDto {
    private Long id;
    private TransportState state;
    private String regNumber;
    private TransportType type;
}
