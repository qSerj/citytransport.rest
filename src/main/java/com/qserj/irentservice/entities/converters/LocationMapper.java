package com.qserj.irentservice.entities.converters;

import com.qserj.irentservice.entities.dto.LocationDto;
import com.qserj.irentservice.entities.entity.Location;
import org.mapstruct.Mapper;

@Mapper
public interface LocationMapper {
    LocationDto map(Location loc);
    Location map(LocationDto dto);
}
