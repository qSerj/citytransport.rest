package com.qserj.irentservice.entities.converters;

import com.qserj.irentservice.entities.dto.TripDto;
import com.qserj.irentservice.entities.entity.Trip;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface TripMapper {

    Trip map(TripDto dto);

    @Mapping(source = "state", target = "status")
    @Mapping(source = "startTimestamp", target = "startDateTime")
    @Mapping(source = "endTimestamp", target = "endDateTime")
    @Mapping(source = "transport", target = "vehicle")
    @Mapping(source = "transport.serial", target = "vehicle.regNumber")
    @Mapping(source = "startParkingZone", target = "startParking")
    @Mapping(source = "endParkingZone", target = "finishParking")
    TripDto map(Trip trip);
}
