package com.qserj.irentservice.entities.converters;

import com.qserj.irentservice.entities.dto.ParkingZoneDto;
import com.qserj.irentservice.entities.entity.ParkingZone;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface ParkingZoneMapper {

    @Mapping(source = "location", target = "area.center")
    @Mapping(source = "radius", target = "area.radiusInMeters")
    ParkingZoneDto map(ParkingZone p);

    @Mapping(source = "area.center", target = "location")
    @Mapping(source = "area.radiusInMeters", target = "radius")
    ParkingZone map(ParkingZoneDto d);
}
