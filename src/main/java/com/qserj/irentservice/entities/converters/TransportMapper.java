package com.qserj.irentservice.entities.converters;

import com.qserj.irentservice.entities.dto.TransportAllFieldsDto;
import com.qserj.irentservice.entities.dto.TransportFullDto;
import com.qserj.irentservice.entities.dto.TransportShortDto;
import com.qserj.irentservice.entities.entity.Bicycle;
import com.qserj.irentservice.entities.entity.ElectricScooter;
import com.qserj.irentservice.entities.entity.Transport;
import com.qserj.irentservice.entities.entity.TransportType;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface TransportMapper {

    TransportFullDto ent2FullDto(Transport e);
    Transport fullDto2Ent(TransportFullDto e);

    @Mapping(source = "serial", target = "regNumber")
    TransportShortDto ent2ShortDto(Transport e);
    Transport shortDto2Ent(TransportShortDto e);

    @Mapping(source = "parkingZone", target = "parking")
    @Mapping(source = "parkingZone.location", target = "parking.area.center")
    @Mapping(source = "parkingZone.location", target = "geoPosition")
    @Mapping(source = "serial", target = "regNumber")
    @Mapping(source = "type.type", target = "type")
    @Mapping(source = "state", target = "status")
    TransportAllFieldsDto map(Bicycle t);

    @Mapping(source = "parkingZone", target = "parking")
    @Mapping(source = "parkingZone.location", target = "parking.area.center")
    @Mapping(source = "parkingZone.location", target = "geoPosition")
    @Mapping(source = "serial", target = "regNumber")
    @Mapping(source = "type.type", target = "type")
    @Mapping(source = "state", target = "status")
    @Mapping(source = "maxSpeedKmh", target = "maxKmPerHourSpeed")
    @Mapping(source = "chargeLevelPercent", target = "chargePercent")
    TransportAllFieldsDto map(ElectricScooter t);

    default TransportAllFieldsDto map(Transport t) {
        if (t instanceof Bicycle) {
            return map((Bicycle)t);
        }
        else if (t instanceof ElectricScooter) {
            return map((ElectricScooter)t);
        }
        else {
            throw new IllegalArgumentException();
        }
    }
}
