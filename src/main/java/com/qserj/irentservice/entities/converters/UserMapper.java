package com.qserj.irentservice.entities.converters;

import com.qserj.irentservice.entities.dto.UserFullDto;
import com.qserj.irentservice.entities.dto.UserShortDto;
import com.qserj.irentservice.entities.entity.User;
import org.mapstruct.Mapper;

@Mapper
public interface UserMapper {

    UserFullDto userToUserFullDto(User user);

    User userFullDtoToUser(UserFullDto userFullDto);

    UserShortDto userToUserShortDto(User user);

    User userShortDtoToUser(UserShortDto userShortDto);
}
