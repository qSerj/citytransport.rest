package com.qserj.irentservice.entities.factory;

import com.qserj.irentservice.entities.converters.ParkingZoneMapper;
import com.qserj.irentservice.entities.dto.TransportAllFieldsDto;
import com.qserj.irentservice.entities.entity.Bicycle;
import com.qserj.irentservice.entities.entity.ElectricScooter;
import com.qserj.irentservice.entities.entity.Transport;
import com.qserj.irentservice.entities.entity.TransportType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TransportFactory {

    private final ParkingZoneMapper pzm;

    @Autowired
    public TransportFactory(ParkingZoneMapper pzm) {
        this.pzm = pzm;
    }

    public Transport createFromAllFieldsDto(TransportAllFieldsDto d) {
        Transport t;
        switch (d.getType()) {
            case "BICYCLE":
                t = new Bicycle();
                break;
            case "ELECTRIC_SCOOTER":
                ElectricScooter e = new ElectricScooter();
                e.setChargeLevelPercent(d.getChargePercent());
                e.setMaxSpeedKmh(d.getMaxKmPerHourSpeed());
                t = e;
                break;
            default:
                throw new IllegalArgumentException();
        }
        t.setId(d.getId());
        t.setState(d.getStatus());
        t.setSerial(d.getRegNumber());
        t.setParkingZone(pzm.map(d.getParking()));
        t.setCondition(d.getCondition());
        return t;
    }
}
