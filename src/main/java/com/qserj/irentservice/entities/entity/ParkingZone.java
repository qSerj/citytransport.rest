package com.qserj.irentservice.entities.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name="parking_zone")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ParkingZone {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    private String name;

    @ManyToOne
    @JoinColumn(name="location_id")
    private Location location;

    @Column(name="radius_m")
    private Integer radius;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable( name="parking_zone_transport_type",
        joinColumns = @JoinColumn(name="parking_zone_id"),
        inverseJoinColumns = @JoinColumn(name="transport_type_id"))
    private Set<TransportType> transportTypes;

    @Override
    public String toString() {
        return "ParkingZone{" +
                "id=" + id +
                ", title='" + name + '\'' +
                ", location=" + location +
                ", radius=" + radius +
                ", transportTypes=" + transportTypes +
                '}';
    }
}
