package com.qserj.irentservice.entities.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.Entity;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class ElectricScooter extends Transport {
    private Integer chargeLevelPercent;
    private Integer maxSpeedKmh;
}
