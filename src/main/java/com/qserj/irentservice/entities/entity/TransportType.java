package com.qserj.irentservice.entities.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name="transport_type")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class TransportType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    private String type;

    @Override
    public String toString() {
        return type;
    }
}
