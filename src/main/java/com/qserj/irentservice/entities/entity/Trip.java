package com.qserj.irentservice.entities.entity;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name="trip")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Trip {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Enumerated(EnumType.STRING)
    private TripState state;

    @ManyToOne
    private User user;

    @ManyToOne
    private Transport transport;

    private LocalDateTime startTimestamp;
    private LocalDateTime endTimestamp;

    @ManyToOne
    private ParkingZone startParkingZone;

    @ManyToOne
    private ParkingZone endParkingZone;

    private BigDecimal totalPrice;

    @Override
    public String toString() {
        return "Trip{" +
                "id=" + id +
                ", state=" + state +
                ", user=" + user.getLogin() +
                ", transport=" + transport.getSerial() +
                ", startTimestamp=" + startTimestamp +
                ", endTimestamp=" + endTimestamp +
                ", startParkingZone=" + startParkingZone.getName() +
                ", endParkingZone=" + endParkingZone.getName() +
                '}';
    }
}
