package com.qserj.irentservice.entities.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name="location")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Location {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    private String title;

    private String latitude;

    private String longitude;

    @Override
    public String toString() {
        return String.format("Location: %s (id=%d) lat: %s lon: %s", title, id, latitude, longitude);
    }
}
