package com.qserj.irentservice.entities.entity;

import com.qserj.irentservice.entities.dto.TransportState;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name="transport")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Transport {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Enumerated(EnumType.STRING)
    private TransportState state;

    private String serial;

    @ManyToOne
    @JoinColumn(name="type_id")
    private TransportType type;

    private String condition;

    @ManyToOne
    @JoinColumn(name="parking_zone_id")
    private ParkingZone parkingZone;

    @Override
    public String toString() {
        return "Transport{" +
                "id=" + id +
                ", state='" + state + '\'' +
                ", serial='" + serial + '\'' +
                ", type=" + type +
                ", condition='" + condition + '\'' +
                ", parkingZone=" + parkingZone +
                '}';
    }
}

