package com.qserj.irentservice.entities.entity;

public enum TripState {
    INPROCESS,
    FINISHED
}
