package com.qserj.irentservice.access;

import com.qserj.irentservice.entities.dto.TransportShortDto;
import com.qserj.irentservice.entities.dto.TransportState;
import com.qserj.irentservice.entities.entity.Transport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Arrays;
import java.util.List;

public interface TransportRepository extends JpaRepository<Transport, Long> {
    List<Transport> findAllByState(TransportState state);
    Transport findBySerial(String serial);
}
