package com.qserj.irentservice.access;

import com.qserj.irentservice.entities.entity.Trip;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.stream.Stream;

public interface TripRepository extends JpaRepository<Trip, Long> {
    @Query("select t from Trip t where t.user.id = ?1")
    List<Trip> findByUserId(Long userId);
}
