package com.qserj.irentservice.access;

import com.qserj.irentservice.entities.entity.TransportType;
import org.springframework.data.jpa.repository.JpaRepository;


public interface TransportTypeRepository extends JpaRepository<TransportType, Long> {
}
