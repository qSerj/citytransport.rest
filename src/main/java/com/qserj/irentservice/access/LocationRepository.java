package com.qserj.irentservice.access;

import com.qserj.irentservice.entities.entity.Location;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LocationRepository extends JpaRepository<Location, Long> {
}
