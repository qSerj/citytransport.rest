package com.qserj.irentservice.controller;

import com.qserj.irentservice.entities.dto.TransportAllFieldsDto;
import com.qserj.irentservice.entities.dto.TransportFullDto;
import com.qserj.irentservice.entities.dto.TransportShortDto;
import com.qserj.irentservice.entities.dto.UserFullDto;
import com.qserj.irentservice.service.TransportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/vehicles")
public class TransportController {

    private final TransportService transportService;

    @Autowired
    public TransportController(TransportService transportService) {
        this.transportService = transportService;
    }

    @GetMapping("")
    public List<TransportAllFieldsDto> list() {
        return transportService.findAll();
    }

    @GetMapping("/{id}")
    public TransportAllFieldsDto get(@PathVariable Long id) {
        return transportService.findById(id);
    }

    @PostMapping
    public TransportAllFieldsDto create(@RequestBody TransportAllFieldsDto transport) {
        return transportService.create(transport);
    }

    @PutMapping("/{id}")
    public TransportAllFieldsDto updateUser(@RequestBody TransportAllFieldsDto transport) {
        return transportService.save(transport);
    }

    @DeleteMapping
    public void deleteUser(@RequestBody TransportAllFieldsDto transport) {
        transportService.delete(transport.getId());
    }
}
