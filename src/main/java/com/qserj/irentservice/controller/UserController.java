package com.qserj.irentservice.controller;

import com.qserj.irentservice.entities.dto.UserFullDto;
import com.qserj.irentservice.entities.dto.UserShortDto;
import com.qserj.irentservice.security.CallContext;
import com.qserj.irentservice.security.SecurityContext;
import com.qserj.irentservice.service.UsersServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    UsersServiceImpl usersService;

    @GetMapping("")
    public List<UserShortDto> listUsers() {
        return usersService.findAll();
    }

    @GetMapping("/current")
    public UserShortDto getCurrent() {
        CallContext ctx = SecurityContext.get();
        return UserShortDto.builder().login(ctx.getLogin()).role(ctx.getRole()).balance(new BigDecimal(0)).build();
    }

    @GetMapping("/{id}")
    public UserFullDto getUser(@PathVariable Long id) {
        return usersService.findById(id);
    }

    @PostMapping
    public UserFullDto createUser(@RequestBody @Validated UserFullDto user) {
        return usersService.createUser(user.getLogin(), user.getPassword(), user.getRole());
    }

    @PutMapping("/{id}")
    public UserFullDto updateUser(@RequestBody @Validated UserFullDto user) {
        return usersService.saveUser(user);
    }

    @DeleteMapping
    public void deleteUser(@RequestBody UserFullDto user) {
        usersService.deleteUser(user.getId());
    }
}
