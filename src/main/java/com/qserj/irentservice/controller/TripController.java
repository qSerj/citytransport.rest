package com.qserj.irentservice.controller;

import com.qserj.irentservice.entities.dto.TripDto;
import com.qserj.irentservice.service.TripServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/trips")
public class TripController {
    private final TripServiceImpl tripService;

    @Autowired
    public TripController(TripServiceImpl tripService) {
        this.tripService = tripService;
    }

    @GetMapping("/view")
    public List<TripDto> getTripByUserId() {
        return tripService.findAll();
    }

//    @PostMapping("/{transportId}")
//    public TripDto createTrip(@RequestBody UserShortDto user, @PathVariable Integer transportId) {
//        return tripService.createTrip(user.getId(), transportId);
//    }

    @PostMapping("/close/{tripId}")
    public TripDto closeTrip(@PathVariable Long tripId) {
        return tripService.closeTrip(tripId);
    }
}
