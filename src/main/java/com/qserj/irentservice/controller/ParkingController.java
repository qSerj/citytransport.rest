package com.qserj.irentservice.controller;

import com.qserj.irentservice.entities.dto.ParkingZoneDto;
import com.qserj.irentservice.service.ParkingSerivceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/parkings")
public class ParkingController {
    @Autowired
    ParkingSerivceImpl parkingsService;

    @GetMapping("")
    public List<ParkingZoneDto> listParkings() {
        return parkingsService.findAll();
    }

    @GetMapping("/{id}")
    public ParkingZoneDto getParking(@PathVariable Long id) {
        return parkingsService.findById(id);
    }

    @PostMapping
    public ParkingZoneDto createParking(@RequestBody ParkingZoneDto parking) {
        return parkingsService.createParking(parking);
    }

    @PutMapping("/{id}")
    public ParkingZoneDto updateParking(@RequestBody ParkingZoneDto parking) {
        return parkingsService.saveParking(parking);
    }

    @DeleteMapping
    public void deleteParking(@RequestBody ParkingZoneDto parking) {
        parkingsService.deleteParking(parking.getId());
    }
}
