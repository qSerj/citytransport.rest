package com.qserj.irentservice.error;


import javax.management.loading.MLetContent;

public class BusinessRuntimeException extends RuntimeException {
    private ErrorCodeEnum errorCode;

    public BusinessRuntimeException(ErrorCodeEnum errorCode, Throwable cause, Object ...args) {
        super(errorCode.getMessage(args), cause);
        this.errorCode = errorCode;
    }

    public BusinessRuntimeException(ErrorCodeEnum errorCode, Object ...args) {
        super(errorCode.getMessage(args));
        this.errorCode = errorCode;
    }

    public ErrorCodeEnum getErrorCode() {
        return errorCode;
    }
}
