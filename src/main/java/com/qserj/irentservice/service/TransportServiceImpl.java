package com.qserj.irentservice.service;

import com.qserj.irentservice.access.TransportRepository;
import com.qserj.irentservice.entities.converters.TransportMapper;
import com.qserj.irentservice.entities.dto.TransportAllFieldsDto;
import com.qserj.irentservice.entities.dto.TransportShortDto;
import com.qserj.irentservice.entities.dto.TransportState;
import com.qserj.irentservice.entities.entity.Transport;
import com.qserj.irentservice.entities.factory.TransportFactory;
import com.qserj.irentservice.error.BusinessRuntimeException;
import com.qserj.irentservice.error.ErrorCodeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class TransportServiceImpl implements TransportService {
    private final TransportRepository repository;
    private final TransportMapper mapper;
    private final TransportFactory factory;

    @Autowired
    public TransportServiceImpl(TransportRepository transportRepo, TransportMapper mapper, TransportFactory factory) {
        this.repository = transportRepo;
        this.mapper = mapper;
        this.factory = factory;
    }

    @Override
    public TransportAllFieldsDto create(TransportAllFieldsDto tafDto) {
        return save(tafDto);
    }

    @Override
    public TransportAllFieldsDto save(TransportAllFieldsDto tafDto) {
        Transport t = factory.createFromAllFieldsDto(tafDto);
        Transport saved = repository.save(t);
        return mapper.map(saved);
    }

    @Override
    public void delete(Long id) {
        Transport transport = getById(id);
        //возможно стоит проверить, не связан ли этот транспорт с открытой арендой
        transport.setState(TransportState.DELETED);
        repository.save(transport);
    }

    @Override
    public TransportAllFieldsDto findById(Long id) {
        return mapper.map(getById(id));
    }

    @Override
    public List<TransportAllFieldsDto> findAll() {
        return repository.findAll()
                .stream()
                .map(mapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public List<TransportShortDto> findAllAvail() {
        return repository.findAllByState(TransportState.FREE)
                .stream()
                .map(mapper::ent2ShortDto)
                .collect(Collectors.toList());
    }

    @Override
    public Transport getById(Long id) {
        return repository.findById(id).orElseThrow(() ->
                new BusinessRuntimeException(ErrorCodeEnum.TRANSPORT_NO_FOUND, id));
    }

    @Override
    public boolean avail(Transport transport) {
        return transport.getState() == TransportState.FREE;
    }

    @Override
    public void rent(Transport transport) {
        transport.setState(TransportState.BUSY);
        repository.save(transport);
    }

    @Override
    public void free(Transport transport) {
        transport.setState(TransportState.FREE);
        repository.save(transport);
    }

    @Override
    public TransportShortDto findBySerial(String serial) {
        return mapper.ent2ShortDto(repository.findBySerial(serial));
    }
}
