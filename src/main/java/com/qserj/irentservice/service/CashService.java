package com.qserj.irentservice.service;

import com.qserj.irentservice.entities.entity.TransportType;
import com.qserj.irentservice.entities.entity.Trip;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Service
public interface CashService {
    BigDecimal getTotal(LocalDateTime start, LocalDateTime end, TransportType type);
}
