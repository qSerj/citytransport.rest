package com.qserj.irentservice.service;

import com.qserj.irentservice.access.UserRepository;
import com.qserj.irentservice.entities.converters.UserMapper;
import com.qserj.irentservice.entities.dto.UserFullDto;
import com.qserj.irentservice.entities.dto.UserShortDto;
import com.qserj.irentservice.entities.entity.User;
import com.qserj.irentservice.entities.entity.UserRole;
import com.qserj.irentservice.error.BusinessRuntimeException;
import com.qserj.irentservice.error.ErrorCodeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UsersServiceImpl implements UserService {
    private final UserRepository usersRepository;
    private final UserMapper usersMapper;

    @Autowired
    public UsersServiceImpl(UserRepository usersRepository, UserMapper usersMapper) {
        this.usersRepository = usersRepository;
        this.usersMapper = usersMapper;
    }

    @Override
    public UserFullDto createUser(String login, String password, UserRole role) {
        User user = new User();
        user.setLogin(login);
        user.setPassword(password);
        user.setRole(role);
        usersRepository.save(user);
        return usersMapper.userToUserFullDto(user);
    }

    @Override
    public UserFullDto saveUser(UserFullDto userFullDto) {
        User savedUser = usersRepository.save(usersMapper.userFullDtoToUser(userFullDto));
        return usersMapper.userToUserFullDto(savedUser);
    }

    @Override
    public void deleteUser(Long userId) {
        usersRepository.deleteById(userId);
    }

    @Override
    public UserFullDto findById(Long id) {
        return usersMapper.userToUserFullDto(getById(id));
    }

    @Override
    public UserFullDto findByLogin(String login) {
        User users = usersRepository.findByLogin(login);
        if (users != null) {
            return usersMapper.userToUserFullDto(users);
        }
        return null;
    }

    @Override
    public List<UserShortDto> findAll() {
        return usersRepository.findAll()
                              .stream()
                              .map(usersMapper::userToUserShortDto)
                              .collect(Collectors.toList());
    }

    @Override
    public User getById(Long id) {
        return usersRepository.findById(id).orElseThrow(() ->
                        new BusinessRuntimeException(ErrorCodeEnum.USER_NO_FOUND, id));
    }

    @Override
    public boolean canRent(User user) {
        return user.getBalance().compareTo(BigDecimal.ZERO) > 0;
    }

    @Override
    public boolean UserExistByLogin(String userName) {
        User u = usersRepository.findByLogin(userName);
        return u != null;
    }
}
