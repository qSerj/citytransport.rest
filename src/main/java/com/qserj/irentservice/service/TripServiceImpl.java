package com.qserj.irentservice.service;

import com.qserj.irentservice.access.TripRepository;
import com.qserj.irentservice.entities.converters.TripMapper;
import com.qserj.irentservice.entities.dto.TransportState;
import com.qserj.irentservice.entities.dto.TripDto;
import com.qserj.irentservice.entities.entity.Transport;
import com.qserj.irentservice.entities.entity.Trip;
import com.qserj.irentservice.entities.entity.TripState;
import com.qserj.irentservice.entities.entity.User;
import com.qserj.irentservice.error.BusinessRuntimeException;
import com.qserj.irentservice.error.ErrorCodeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class TripServiceImpl implements TripService {
    private final TripRepository tripRepository;
    private final UserService userService;
    private final TransportService transportService;
    private final TripMapper tripConverter;
    private final CashService cashService;

    @Autowired
    public TripServiceImpl(TripRepository tripRepository,
                           UserService userService,
                           TransportService transportService, TripMapper tripConverter, CashService cashService) {
        this.tripRepository = tripRepository;
        this.userService = userService;
        this.transportService = transportService;
        this.tripConverter = tripConverter;
        this.cashService = cashService;
    }

    @Override
    @Transactional
    public synchronized TripDto createTrip(Long userId, Long transportId) {
        Trip createdTrip;

        User user = userService.getById(userId);
        Transport transport = transportService.getById(transportId);

        if (!userService.canRent(user)) {
            throw new BusinessRuntimeException(ErrorCodeEnum.USER_CANT_RENT, user.getId());
        }

        if (!transportService.avail(transport)) {
            throw new BusinessRuntimeException(ErrorCodeEnum.TRANSPORT_NOT_AVAILABLE, transport.getId());
        }

        Trip trip = new Trip();
        trip.setState(TripState.INPROCESS);
        trip.setUser(user);
        trip.setTransport(transport);
        trip.setStartTimestamp(LocalDateTime.now());
        trip.setStartParkingZone(transport.getParkingZone());
        transport.setState(TransportState.BUSY);
        createdTrip = tripRepository.save(trip);

        //transportService.rent(transport);

        return tripConverter.map(createdTrip);
    }

    @Override
    @Transactional
    public synchronized TripDto closeTrip(Long tripId) {
        Trip savedTrip = null;
        Trip trip = getById(tripId);
        User user = trip.getUser();
        Transport transport = trip.getTransport();
        LocalDateTime nowTime = LocalDateTime.now();
        BigDecimal total = cashService.getTotal(trip.getStartTimestamp(), LocalDateTime.now(), trip.getTransport().getType());
        int result = user.getBalance().compareTo(total);
        if (result >= 0) {
            trip.setEndParkingZone(trip.getStartParkingZone());
            trip.setEndTimestamp(LocalDateTime.now());
            trip.setTotalPrice(total);
            trip.setState(TripState.FINISHED);
            BigDecimal decreasedBalance = user.getBalance().subtract(total);
            user.setBalance(decreasedBalance);
            transport.setState(TransportState.FREE);
            savedTrip = tripRepository.save(trip);
        }
        return tripConverter.map(savedTrip);
    }

    @Override
    public Trip getById(Long id) {
        return tripRepository.getById(id);
    }

    @Override
    public TripDto saveTrip(TripDto trip) {
        return null;
    }

    @Override
    public void deleteTrip(Long userId) {
    }

    @Override
    public TripDto findById(Long id) {
        return null;
    }

    @Override
    public List<TripDto> findByUserId(Long id) {
        return tripRepository.findByUserId(id)
                .stream()
                .map(tripConverter::map)
                .collect(Collectors.toList());
    }

    @Override
    public List<TripDto> findAll() {
        return tripRepository.findAll()
                .stream()
                .map(tripConverter::map)
                .collect(Collectors.toList());
    }
}
