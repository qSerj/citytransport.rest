package com.qserj.irentservice.service;

import com.qserj.irentservice.entities.dto.ParkingZoneDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ParkingService {

    ParkingZoneDto createParking(ParkingZoneDto parking);

    ParkingZoneDto saveParking(ParkingZoneDto parking);

    void deleteParking(Long Id);

    ParkingZoneDto findById(Long id);

    List<ParkingZoneDto> findAll();
}
