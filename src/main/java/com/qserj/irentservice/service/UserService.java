package com.qserj.irentservice.service;

import com.qserj.irentservice.entities.dto.UserFullDto;
import com.qserj.irentservice.entities.dto.UserShortDto;
import com.qserj.irentservice.entities.entity.User;
import com.qserj.irentservice.entities.entity.UserRole;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {

    UserFullDto createUser(String login, String password, UserRole role);

    UserFullDto saveUser(UserFullDto user);

    void deleteUser(Long userId);

    UserFullDto findById(Long id);

    UserFullDto findByLogin(String login);

    List<UserShortDto> findAll();

    User getById(Long id);

    boolean canRent(User user);

    boolean UserExistByLogin(String userName);
}
