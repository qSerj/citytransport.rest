package com.qserj.irentservice.service;

import com.qserj.irentservice.entities.dto.TransportAllFieldsDto;
import com.qserj.irentservice.entities.dto.TransportFullDto;
import com.qserj.irentservice.entities.dto.TransportShortDto;
import com.qserj.irentservice.entities.entity.Transport;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface TransportService {

    TransportAllFieldsDto create(TransportAllFieldsDto transportAllFieldsDto);

    TransportAllFieldsDto save(TransportAllFieldsDto transportAllFieldsDto);

    void delete(Long Id);

    TransportAllFieldsDto findById(Long id);

    List<TransportAllFieldsDto> findAll();

    List<TransportShortDto> findAllAvail();

    Transport getById(Long id);

    boolean avail(Transport transport);

    void rent(Transport transport);

    void free(Transport transport);

    TransportShortDto findBySerial(String string);
}
