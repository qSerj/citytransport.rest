package com.qserj.irentservice.service;

import com.qserj.irentservice.access.LocationRepository;
import com.qserj.irentservice.access.ParkingZoneRepository;
import com.qserj.irentservice.entities.converters.LocationMapper;
import com.qserj.irentservice.entities.converters.ParkingZoneMapper;
import com.qserj.irentservice.entities.dto.ParkingZoneDto;
import com.qserj.irentservice.entities.entity.ParkingZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ParkingSerivceImpl implements ParkingService {

    private final ParkingZoneRepository parkingZoneRepository;
    private final LocationRepository locationRepository;
    private final ParkingZoneMapper parkingConverter;
    private final LocationMapper locationConverter;

    @Autowired
    public ParkingSerivceImpl(ParkingZoneRepository parkingZoneRepository, LocationRepository locationRepository,
                              ParkingZoneMapper parkingConverter, LocationMapper converter) {
        this.parkingZoneRepository = parkingZoneRepository;
        this.locationRepository = locationRepository;
        this.parkingConverter = parkingConverter;
        this.locationConverter = converter;
    }

    @Override
    public ParkingZoneDto createParking(ParkingZoneDto parking) {
        ParkingZone pkz = new ParkingZone();
        pkz.setName(parking.getName());
        pkz.setLocation(locationConverter.map(parking.getArea().getCenter()));
        locationRepository.save(pkz.getLocation());
        pkz.setRadius(parking.getArea().getRadiusInMeters());
        return parkingConverter.map(parkingZoneRepository.save(pkz));
    }

    @Override
    public ParkingZoneDto saveParking(ParkingZoneDto parking) {
        ParkingZone savedParking = parkingZoneRepository.save(parkingConverter.map(parking));
        return parkingConverter.map(savedParking);
    }

    @Override
    public void deleteParking(Long id) {
        parkingZoneRepository.deleteById(id);
    }

    @Override
    public ParkingZoneDto findById(Long id) {
        return parkingConverter.map(parkingZoneRepository.findById(id).orElseThrow());
    }

    @Override
    public List<ParkingZoneDto> findAll() {
        return parkingZoneRepository.findAll()
                                    .stream()
                                    .map(parkingConverter::map)
                                    .collect(Collectors.toList());
    }
}
