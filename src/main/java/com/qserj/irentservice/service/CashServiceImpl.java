package com.qserj.irentservice.service;

import com.qserj.irentservice.entities.entity.TransportType;
import com.qserj.irentservice.entities.entity.Trip;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

@Component
public class CashServiceImpl implements CashService {
    @Override
    public BigDecimal getTotal(LocalDateTime start, LocalDateTime end, TransportType type) {
        BigDecimal rublsPerSecond;

        if (type.getType().equals("BICYCLE")) {
            rublsPerSecond = new BigDecimal("0.03");
        }
        else {
            rublsPerSecond = new BigDecimal("0.1");
        }

        BigDecimal secs = new BigDecimal(start.until(end, ChronoUnit.SECONDS));

        return secs.multiply(rublsPerSecond);
    }
}
