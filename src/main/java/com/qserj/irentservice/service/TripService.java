package com.qserj.irentservice.service;

import com.qserj.irentservice.entities.dto.TripDto;
import com.qserj.irentservice.entities.entity.Trip;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface TripService {

    TripDto createTrip(Long userId, Long transportId);

    TripDto saveTrip(TripDto trip);

    void deleteTrip(Long userId);

    TripDto findById(Long id);

    List<TripDto> findByUserId(Long id);

    List<TripDto> findAll();

    TripDto closeTrip(Long tripId);

    Trip getById(Long id);
}
