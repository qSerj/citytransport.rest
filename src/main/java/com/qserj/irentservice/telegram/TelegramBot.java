/*
/start – регистрация пользователя (клиента). При регистрации клиенту начисляется исходный баланс, который задается через настройки +

/info – текущая информация по пользователю – баланс, текущие поездки

/search (find) – поиск свободных ТС. Опционально параметров передается тип ТС

/trip – начало аренды

/finish – окончание аренды
 */

package com.qserj.irentservice.telegram;

import com.qserj.irentservice.telegram.commands.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.function.ServerResponse;
import org.telegram.telegrambots.extensions.bots.commandbot.TelegramLongPollingCommandBot;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.List;

@Slf4j
@Component
public class TelegramBot extends TelegramLongPollingCommandBot {
    @Value("${bot.name}")
    private String botUsername;
    @Value("${bot.token}")
    private String botToken;

    @Autowired
    public TelegramBot(List<BaseCommand> commands) {
        for (BaseCommand command : commands) {
            this.register(command);
        }
    }

    @Override
    public String getBotUsername() {
        return botUsername;
    }

    @Override
    public void processNonCommandUpdate(Update update) {
    }

    @Override
    public String getBotToken() {
        return botToken;
    }

    @Override
    public void onRegister() {
        super.onRegister();
    }

    @Override
    public void onUpdatesReceived(List<Update> updates) {
        super.onUpdatesReceived(updates);
    }
}
