package com.qserj.irentservice.telegram.commands;

import com.qserj.irentservice.entities.dto.TripDto;
import com.qserj.irentservice.service.TransportService;
import com.qserj.irentservice.service.TripService;
import com.qserj.irentservice.service.UserService;
import com.qserj.irentservice.telegram.TelegramUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

import java.time.temporal.ChronoUnit;

@Slf4j
@Component
public class FinishCommand extends BaseCommand {
    private final TelegramUtils utils;
    private final TripService tripService;

    @Autowired
    public FinishCommand(TelegramUtils utils, TripService tripService) {
        super("finish", "finish command", utils);
        this.utils = utils;
        this.tripService = tripService;
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings) {
        log.info("Telegram FINISH command received");
        try {
            String message;
            Long tripId = Long.parseLong(strings[0]);
            TripDto finishedTripDto = tripService.closeTrip(tripId);
            if (finishedTripDto != null) {
                message = String.format("Вы успешно завершили поездку %d, её длительность составила %sсек., стоимость %sр.",
                        finishedTripDto.getId(),
                        finishedTripDto.getStartDateTime().until(finishedTripDto.getEndDateTime(), ChronoUnit.SECONDS),
                        finishedTripDto.getTotalPrice().toString());
            }
            else {
                message = "Что-то пошло не так. Убедитесь, что вводите правильный id поездки (проверьте командой /info).";
            }
            utils.send(absSender, chat.getId(), message, ParseMode.HTML,false);
        }
        catch (Exception ex) {
            log.error("Ошибка обработки команды finish", ex);
        }
    }
}
