package com.qserj.irentservice.telegram.commands;

import com.qserj.irentservice.entities.dto.UserFullDto;
import com.qserj.irentservice.entities.entity.UserRole;
import com.qserj.irentservice.service.UserService;
import com.qserj.irentservice.telegram.TelegramUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

@Slf4j
@Component
public class StartCommand extends BaseCommand {

    private final TelegramUtils utils;
    private final UserService userService;

    @Autowired
    public StartCommand(TelegramUtils utils, UserService userService) {
        super("start", "start command", utils);
        this.utils = utils;
        this.userService = userService;
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings) {
        log.info("Telegram START command received");
        try {
            utils.send(absSender, chat.getId(), "Добро пожаловать.", ParseMode.HTML, false);
            if (userService.UserExistByLogin(user.getUserName())) {
                utils.send(absSender, chat.getId(), String.format("Здравствуйте %s", user.getUserName()), ParseMode.HTML, false);
            }
            else {
                UserFullDto userFullDto = userService.createUser(user.getUserName(), "55555", UserRole.USER);
                userFullDto.setBalance(1000);
                userFullDto = userService.saveUser(userFullDto);
                utils.send(absSender,
                        chat.getId(),
                        String.format("Добро пожаловать %s, Ваш временный пароль %s, Ваш баланс %dр",
                                userFullDto.getLogin(), userFullDto.getPassword(), userFullDto.getBalance()),
                        ParseMode.HTML,
                        false);
            }
        }
        catch (Exception ex) {
            log.error("Ошибка обработки команды start", ex);
        }
    }
}
