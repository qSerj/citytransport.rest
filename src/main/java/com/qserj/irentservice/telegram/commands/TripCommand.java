package com.qserj.irentservice.telegram.commands;

import com.qserj.irentservice.entities.dto.TransportShortDto;
import com.qserj.irentservice.entities.dto.TripDto;
import com.qserj.irentservice.entities.dto.UserFullDto;
import com.qserj.irentservice.service.TransportService;
import com.qserj.irentservice.service.TripService;
import com.qserj.irentservice.service.UserService;
import com.qserj.irentservice.telegram.TelegramUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

@Slf4j
@Component
public class TripCommand extends BaseCommand {

    private final TelegramUtils utils;
    private final UserService userService;
    private final TransportService transportService;
    private final TripService tripService;

    @Autowired
    public TripCommand(TelegramUtils utils, TelegramUtils utils1, UserService userService, TransportService transportService, TripService tripService) {
        super("trip", "trip command", utils);
        this.utils = utils1;
        this.userService = userService;
        this.transportService = transportService;
        this.tripService = tripService;
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings) {
        log.info("Telegram TRIP command received");
        try {
            String message;
            UserFullDto usr = userService.findByLogin(user.getUserName());
            TransportShortDto tsd = transportService.findBySerial(strings[0]);
            TripDto createdTripDto = tripService.createTrip(usr.getId(), tsd.getId());
            if (createdTripDto != null) {
                message = String.format("Поздравляю, вы арендовали %s, ваша аренда началась в %s", tsd.getRegNumber(), createdTripDto.getStartDateTime());
            }
            else {
                message = String.format("Простите, что-то пошло не так, арендовать %s не получилось", tsd.getRegNumber());
            }
            utils.send(absSender, chat.getId(), message, ParseMode.HTML,false);
        }
        catch (Exception ex) {
            log.error("Ошибка обработки команды trip", ex);
        }
    }
}
