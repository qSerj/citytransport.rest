package com.qserj.irentservice.telegram.commands;

import com.qserj.irentservice.entities.dto.TransportShortDto;
import com.qserj.irentservice.service.TransportService;
import com.qserj.irentservice.telegram.TelegramUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

import java.util.List;

@Slf4j
@Component
public class SearchCommand extends BaseCommand {

    private final TransportService transportService;

    public SearchCommand(TelegramUtils utils, TransportService transportService) {
        super("search", "search command", utils);
        this.transportService = transportService;
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings) {
        log.info("Telegram SEARCH command received");
        try {

            StringBuilder sb = new StringBuilder();

            List<TransportShortDto> transportList = transportService.findAllAvail();

            if (transportList != null && !transportList.isEmpty()) {
                for (TransportShortDto tsd : transportList) {
                    sb.append(String.format("Доступен %s '%s'\n(для создания аренты дайте команду /trip '%s')\n\n", tsd.getType(), tsd.getRegNumber(), tsd.getRegNumber()));
                }
            }
            else {
                sb.append("Похоже все разобрали. Заходите позже.");
            }

            utils.send(absSender, chat.getId(), sb.toString(), ParseMode.HTML, false);
        }
        catch (Exception ex) {
            log.error("Ошибка обработки команды search", ex);
        }
    }
}
