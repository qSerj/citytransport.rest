package com.qserj.irentservice.telegram.commands;

import com.qserj.irentservice.telegram.TelegramUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.BotCommand;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

@Slf4j
public abstract class BaseCommand extends BotCommand {

    protected final TelegramUtils utils;

    public BaseCommand(String commandIdentifier, String description, TelegramUtils utils) {
        super(commandIdentifier, description);
        this.utils = utils;
    }
}
