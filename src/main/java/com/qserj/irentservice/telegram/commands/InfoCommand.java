package com.qserj.irentservice.telegram.commands;

import com.qserj.irentservice.entities.dto.TripDto;
import com.qserj.irentservice.entities.dto.UserFullDto;
import com.qserj.irentservice.service.TripService;
import com.qserj.irentservice.service.UserService;
import com.qserj.irentservice.telegram.TelegramUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

import java.util.List;

@Slf4j
@Component
public class InfoCommand  extends BaseCommand  {

    private final TelegramUtils utils;
    private final UserService userService;
    private final TripService tripService;

    @Autowired
    public InfoCommand(TelegramUtils utils, TelegramUtils utils1, UserService userService, TripService tripService) {
        super("info", "info command", utils);
        this.utils = utils1;
        this.userService = userService;
        this.tripService = tripService;
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings) {
        log.info("Telegram INFO command received");
        try {
            UserFullDto userDto = userService.findByLogin(user.getUserName());

            StringBuilder sb = new StringBuilder();

            if (userDto == null) {
                sb.append(String.format("Пользователь %s не найден. Воспользуйтесь командой /start для регистрации.", user.getUserName()));
            }
            else {
                sb.append(String.format("Ваш логин: %s\n", user.getUserName()));
                sb.append(String.format("Ваш пароль: %s\n", userDto.getPassword()));
                sb.append(String.format("Ваш балланс: %s\n", userDto.getBalance()));

                List<TripDto> trips = null;

                try {
                    trips = tripService.findByUserId(userDto.getId());
                } catch (Exception e) {
                    log.error("Ошибка получения списка поездок", e);
                    sb.append("Похоже у Вас еще не было поездок\n");
                }

                if (trips != null) {
                    for (TripDto t : trips) {
                        sb.append(String.format("Поездка %s, транспорт %s, начало %s",
                                t.getId(), t.getVehicle().getRegNumber(), t.getStartDateTime()));
                        if (t.getEndDateTime() != null) {
                            sb.append(String.format(", окончание %s, стоимость %s",
                                    t.getEndDateTime(), t.getTotalPrice()));
                        }
                        else {
                            sb.append(", в процессе.");
                        }
                        sb.append("\n");
                    }
                }
            }

            utils.send(absSender, chat.getId(), sb.toString(), ParseMode.HTML, false);
        }
        catch (Exception ex) {
            log.error("Ошибка обработки команды info", ex);
        }
    }
}
