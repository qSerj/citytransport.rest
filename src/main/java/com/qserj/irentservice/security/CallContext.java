package com.qserj.irentservice.security;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CallContext {
    private Integer userId;
    private String login;
    private String role;
}
