package com.qserj.irentservice;

import com.qserj.irentservice.access.ParkingZoneRepository;
import com.qserj.irentservice.access.TransportRepository;
import com.qserj.irentservice.access.TripRepository;
import com.qserj.irentservice.access.UserRepository;
import com.qserj.irentservice.entities.entity.Transport;
import com.qserj.irentservice.entities.entity.Trip;
import com.qserj.irentservice.entities.entity.User;
import com.qserj.irentservice.entities.entity.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.List;

@SpringBootApplication
public class IrentServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(IrentServiceApplication.class, args);
	}

//	@Bean
//	public CommandLineRunner run4Users(UserRepository repository) {
//		return args -> {
//			//...
//		};
//	}

}
